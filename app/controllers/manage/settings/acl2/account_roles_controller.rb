class Manage::Settings::Acl2::AccountRolesController < ActionController::Base
  before_action :find_account, except: [:welcome, :accounts, :accounts_show, :index ]

  def welcome
    
  end

  def accounts
    @accounts = current_user.accounts
  end

  def accounts_show
    
  end
  
  def save_role
    binding.pry
    if params[:gb_admin_role].present?
      if params[:gb_admin_role][:role].count == 1
        GbAdminRole.create! user_id: @account.user.id, role: params[:gb_admin_role][:role].first
        flash[:success] = "Record Saved!"
        redirect_to account_roles_accounts_path
      elsif params[:gb_admin_role][:role].count == 2
        GbAdminRole.create! user_id: @account.user.id, role: "ROLE_ADMIN"
        flash[:success] = "Record Saved!"
        redirect_to account_roles_accounts_path
      else
        redirect_to account_roles_accounts_path
      end
    else
      redirect_to account_roles_accounts_path
      flash[:error] = "Please Fill in correctly!"
    end
  end

  def account_roles
    @account_roles = AccountRole.all
  end

  def index
    @account_roles = @account.account_roles
  end

  def new
    @account_role = current_account.account_roles.new
  end

  def create
    @account_role = current_account.account_roles.new(account_role_params)
    if @account_role.save
      flash[:notice] = 'Role created.'
      redirect_to manage_settings_acl2_account_roles_path
    else
      render :new
    end
  end

  def update
    @account_role = current_account.account_roles.find params[:id]
    @account_role.update_attributes account_role_params
    flash[:notice] = 'Role updated'
    redirect_to manage_settings_acl2_account_roles_path
  end

  def edit
    @account_role = current_account.account_roles.find params[:id]
  end

  def destroy
    @account_role = @account.account_roles.find params[:id]
    current_account.account_roles.delete(@account_role)
    flash[:notice] = 'Role removed.'
    redirect_to manage_settings_acl2_account_roles_path
  end

  def rights
    @account_role = @account.account_roles.find params[:id]
  end

  def update_rights
    binding.pry
    @account_role = @account.account_roles.find params[:id]
    params[:right_ids].each do |right_id, result|
      if result.to_s=='true'
        AccountRoleRight.find_or_create_by(right_id: right_id, account_role_id: params[:id])
      else
        AccountRoleRight.where(right_id: right_id, account_role_id: params[:id]).delete_all
      end
    end
    flash[:notice] = 'Rights updated.'
    redirect_to manage_settings_acl2_account_roles_path
  end

  def users
    @account_role = current_account.account_roles.find params[:id]
  end

  def update_users
    @account_role = current_account.account_roles.find params[:id]
    params[:user_ids].each do |user_id, result|
      if result.to_s=='true'
        UserAccountRole.find_or_create_by(user_id: user_id, account_role_id: params[:id])
      else
        UserAccountRole.where(user_id: user_id, account_role_id: params[:id]).delete_all
      end
    end
    flash[:notice] = 'Users updated.'
    redirect_to manage_settings_acl2_account_roles_path
  end

  private

    def account_role_params
      params.require(:account_role).permit(:name, :description)
    end

    def find_account
      @account = AccountRole.find(params[:id]).account
    end


end
