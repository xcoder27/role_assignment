class Account < ActiveRecord::Base
  #has_settings :integrations, :configuration
  #include ::Concerns::Account::Integrations
  #include ::Concerns::Account::Stats
  #include ::Concerns::Account::Observer
  #include ::Concerns::Account::AccreditationVersioning
  #acts_as_alertable
  #seo_friendly
  
  #has_many :seo_settings, dependent: :destroybelongs_to :owner, :class_name => "User"
  #accepts_nested_attributes_for :owner
  has_many :account_roles, dependent: :destroy
  belongs_to :user
  def theme_config key=''
    self.theme.config.parse key
  end

  def tc key=''
    #binding.pry
    self.theme.config.parse key
  end

  def tc_or_demo2 key=''
    tc(key) or ThemeConfig.demo2.parse(key)
  end

  #scope :active, -> { where(active: true) }
  #scope :inactive, -> { where(active: false) }
  #has_attached_file :logo, :styles => { :large => "400x400", :medium => "200x200", :thumb => "50x50" }
  #do_not_validate_attachment_file_type :logo
  #validates_format_of :subdomain, :with => /\A[\w\-]+\Z/i, :message => "is not allowed. Please choose another subdomain."
  #validates :subdomain, :presence => true, :uniqueness => true
  #validates :domain, :uniqueness => true, :allow_blank => true
  #EXCLUDED_SUBDOMAINS = %w(admin)
  #validates_exclusion_of :subdomain, :in => EXCLUDED_SUBDOMAINS, :message => "is not allowed. Please choose another subdomain."
  #has_many :account_members, dependent: :destroy
  #has_many :role_members, :foreign_key => 'account_id', :class_name => 'UserRole', dependent: :destroy
  #has_many :disclosure_sections, through: :deals, source: :sections
  
  # has_many :members, through: :account_members, source: :user do
  #   def search(params)
  #     if params[:query].present?
  #       where('first_name LIKE ? OR last_name LIKE ? OR email LIKE ?', "%#{ params[:query]}%", "%#{ params[:query] }%", "%#{ params[:query] }%")
  #     else
  #       all
  #     end
  #   end

  #   def followers_of user
  #     follower_ids = user.incoming_follows.map(&:follower_id).where(id: follower_ids)
  #   end
  # end

  #has_many :account_admins, dependent: :destroy
  #has_many :admins, through: :account_admins, source: :user
  #has_many :approvals, dependent: :destroy
  #has_many :accredify_users, dependent: :destroy
  #has_many :account_site_features, dependent: :destroy
  #has_many :site_features, through: :account_site_features

  # def features
  #   @features ||= site_features.pluck :name
  # end

  #has_many :custom_forms, dependent: :destroyhas_many :invites, dependent: :destroy
  #has_many :deals, dependent: :destroy
  #has_many :custom_templates, dependent: :destroy
  #has_many :pages, dependent: :destroy
  #has_many :partials, dependent: :destroy
  #has_many :popups, dependent: :destroy
  #has_many :email_templates, dependent: :destroy
  #has_many :user_account_settings, dependent: :destroy
  #has_many :account_settings, dependent: :destroy
  #has_many :user_accreditation_methods, dependent: :destroy
  #has_many :loans, dependent: :destroy
  #has_many :categories, dependent: :destroy
  #has_many :filter_types, dependent: :destroy
  #has_many :filters, dependent: :destroy
  #belongs_to :theme
  #belongs_to :marketplace
  #has_many :system_notifications
  #has_many :notifications, :through => :system_notifications, :source => :receiver, :source_type => 'Account'
  # has_many :system_notifications, :foreign_key => 'receiver_id' do
  #   def mark_all_as_read
  #     all.each do | notification|
  #       notification.mark_as_read
  #     end
  #   end
  # end
  # has_many :articles, dependent: :destroy
  # has_many :notes, dependent: :destroy
  # has_many :contacts do
  #   def search(params)
  #     if params[:query].present?
  #       where('first_name LIKE ? OR last_name LIKE ? OR email LIKE ? OR company LIKE ? OR title LIKE ?', "%#{ params[:query] }%", "%#{ params[:query] }%", "%#{ params[:query]}%", "%#{ params[:query] }%", "%#{ params[:query] }%")
  #     else
  #       all
  #     end
  #   end
  # end

  #has_many :account_investments, :through => :deals, :source => :investments
  #has_many :user_account_locks, dependent: :destroy
  #after_initialize :defaults, unless: :persisted?
  # after_create :after_create_account

  # def after_create_account
  #   self.pages.create( {:title => "Privacy Policy", :url_handle => 'privacy', content:
  #   Account.demo2.pages.where(url_handle: 'privacy').first.try(:content)})
  #   self.pages.create( {:title => "Terms of Use", :url_handle => 'terms',
  #   content:
  #   Account.demo2.pages.where(url_handle: 'terms').first.try(:content)})
  #   self.pages.create( {:title => "About Us",
  #   :url_handle => 'about',
  #   content:
  #   Account.demo2.pages.where(url_handle: 'about').first.try(:content)})
  #   self.pages.create( {:title => "FAQs",
  #   :url_handle => 'faq',
  #   content:
  #   Account.demo2.pages.where(url_handle: 'faq').first.try(:content)})
  #   self.partials.create({:title => "Disclaimer", :url_handle => 'disclaimer', content:
  #   Account.demo2.partials.where(url_handle: 'disclaimer').first.try(:content)})
  # end

  #has_many :synapsepay_accounts, dependent: :destroy
  # def synapsepay_escrow_accounts
  #   synapsepay_accounts.where(owner_type: 'Deal')
  # end
  # def synapsepay_investor_accounts
  #   synapsepay_accounts.where(owner_type: 'User')
  # end
  # def defaults
  #   self.theme = Theme.where(:path => 'demo2').first
  # end
  # before_validation do 
  #   self.subdomain = subdomain.to_s.downcase
  # end
  # def handler
  #   RequestStore.store[:account_handler]
  # end
  # def self.create_with_owner(params={})
  #   account = new(params)
  #   if account.save
  #     account.admins << account.owner
  #     account.members << account.owner
  #     account.admins << Account.gbadmins rescue nil
  #     account.members << Account.gbadmins rescue nil
  #     role = AccountRole.create name: 'Owner', account_id: account.id
  #     UserAccountRole.create account_role_id: role.id, user_id: account.owner.id, created_by:
  #     account.owner.id
  #     Right.pluck(:id, :category).each do | right_id, category|
  #     next if category.to_s.ends_with?('_custom_forms')
  #     AccountRoleRight.create account_role_id: role.id, right_id: right_id
  #     end
  #   end
  # account
  # end
  # def url(with_http = true)
  #   str = with_http ? "http://" : ""
  #   if domain.present?
  #     str << "#{domain}"
  #     elsestr << "#{subdomain}.#{BASE_DOMAIN}"
  #   end
  #   str
  # end
  # def environment_aware_url(with_http = true)
  #   if Rails.env == 'production'
  #     url(with_http)
  #   else
  #     "http://#{subdomain}.#{BASE_DOMAIN}"
  #   end
  # end
  # def self.gbadmin_emails
  #   @gbadmin_emails ||= %w(amol@groundbreaker.co jake@groundbreaker.co
  #   atlas@groundbreaker.co ed@groundbreaker.co walt@groundbreaker.co
  #   everton@groundbreaker.co)
  # end
  # def self.gbadmins
  #   @gbadmins ||= User.where(email: gbadmin_emails)
  # end
  # def self.gb_crew
  #   @gb_crew ||= User.where(email: %w(amol@groundbreaker.co jake@groundbreaker.co
  #   atlas@groundbreaker.co tambe.amol.balkrishna@gmail.com ed@groundbreaker.co
  #   walt@groundbreaker.co everton@groundbreaker.co))
  # end
  # def client_admins
  #   self.admins.where(' users.id not IN (?)', self.class.gb_crew.pluck(:id))
  # end
  # def configuration
  #   @configuration ||= Account::Configuration.new(self)
  # end
  # def config
  #   @configuration ||= Account::Configuration.new(self)
  # end
  # def idashboard_pathtc('idashboard_path') || '/idashboard'
  # end
  # def manage_members_table(_members=nil)
  #   _members ||= self.members
  #   @manage_members_table ||= MembersTable.new self, _members
  # end
  # def manage_deals_table(_deals=nil)
  #   _deals ||= self.deals
  #   @manage_deals_table ||= DealsTable.new self, _deals
  # end
  # def manage_members_profile_card user
  #   @manage_members_profile_card ||= MemberProfileCard.new self, user
  # end
  # def followers
  #   User.where id: follower_ids
  # end
  # def follower_ids
  #   deals.map{|deal| deal.follower_ids}.flatten.uniq
  # end
  # def investors
  #   User.where id: investor_ids
  # end
  # def investor_ids
  #   investments.map{|f| f.user_id }.compact.uniq
  # end
  # def visitor_ids
  #   deals.map{|deal| deal.visitor_ids}.flatten.uniq
  # end
  # def visitors
  #   User.where id: visitor_ids
  # end

  # def investments
  #   Investment.where(deal_id: deals.pluck(:id))
  # end

  # def unread_notifications
  #   @unread_notifications ||= system_notifications.unread
  # end

  # def property_types
  #   @property_types ||= deals.published.pluck(:property_type).compact.uniq.sort
  # end

  # def last_month_commentsreturn @last_month_comments if @last_month_comments.present?
  #   deal_ids = self.deals.pluck :id
  #   comments = Comment.where(commentable_type: 'Deal').where(commentable_id:
  #   deal_ids).where('updated_at > ? ', DateTime.now - 30.days ).reorder('updated_at desc')
  #   @last_month_comments = comments.select{|c| c.approved?}.uniq{|c| c.commentable_id }
  # end
  # def deals_nearly_funded
  #   @deals_nearly_funded ||= deals.published.select { |deal| deal.percent_funded >= 75 }
  # end

end