Rails.application.routes.draw do

  devise_for :users

  get 'account_roles_accounts', to: 'account_roles#accounts'
  get 'rights', to: 'account_roles#rights'
  get 'account_roles', to: 'account_roles#account_roles'

  namespace :manage do
    namespace :settings do
      namespace :acl2 do
        resources :account_roles do
          member do
            get :rights
            put :update_rights
            get :save_role

            get :users
            put :update_users
          end
        end
      end
    end
  end

  root "account_roles#welcome"
end
