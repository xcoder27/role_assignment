# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

  user = User.create(username: "baria", password: "password", email: "b@b.com")
  user.accounts.create! name: "Account # 1"
  user.accounts.create! name: "Account # 2"
  user.accounts.create! name: "Account # 3"
  user.accounts.last.account_roles.create! name: "Account Role 1"
  user.accounts.last.account_roles.create! name: "Account Role 2"
  user.accounts.last.account_roles.create! name: "Account Role 3"
  user.accounts.first.account_roles.create! name: "Account Role 1"
  user.accounts.first.account_roles.create! name: "Account Role 2"
  user.accounts.first.account_roles.create! name: "Account Role 3"  