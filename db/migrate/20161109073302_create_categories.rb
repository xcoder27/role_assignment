class CreateCategories < ActiveRecord::Migration[5.0]
  def change
    create_table "categories", force: true do |t|
      t.string "name"
      t.text "description"
      t.integer "account_id"
      t.datetime "created_at"
      t.datetime "updated_at"
      t.string "image_file_name"
      t.string "image_content_type"
      t.integer "image_file_size"
      t.datetime "image_updated_at"
    end
  end
end
