class CreateUserRoles < ActiveRecord::Migration[5.0]
  def change
    create_table "user_roles", force: true do |t|
      t.integer "user_id"
      t.integer "account_id"
      t.string "role"
      t.datetime "created_at"
      t.datetime "updated_at"
    end
  end
end