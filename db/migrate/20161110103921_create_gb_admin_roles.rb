class CreateGbAdminRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :gb_admin_roles do |t|
      t.references :user, foreign_key: true
      t.string :role
      t.timestamps
    end
  end
end
