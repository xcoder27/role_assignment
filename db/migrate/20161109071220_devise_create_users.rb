class DeviseCreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table "users", force: true do |t|
      #Database authenticatable
      t.string :email, :null => false, :default => ""
      t.string :encrypted_password, :null => false, :default => ""

      # Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      # Trackable & Confrimable
      t.integer  :sign_in_count, :default => 0
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip

      t.string   :confirmation_token
      t.datetime :confirmed_at
      t.datetime :confirmation_sent_at
      t.string   :confirmation_token
      t.datetime :remember_created_at
      t.string "username"
      t.string "avatar_file_name"
      t.string "avatar_content_type"
      t.integer "avatar_file_size"
      t.datetime "avatar_updated_at"
      t.text "about_me"
      t.string "first_name"
      t.string "middle_name"
      t.string "last_name"
      t.string "title"
      t.string "phone"
      t.string "country", default: "US"
      t.boolean "admin", default: false
      t.string "encrypted_ssn"
      t.string "encrypted_dob"
      t.string "encrypted_ssn_salt"
      t.string "encrypted_ssn_iv"
      t.string "encrypted_dob_salt"
      t.string "encrypted_dob_iv"
      t.string "vesting"
      t.string "screen_name"
      t.string "occupation"
      t.string "url"
      t.string "last_sign_in_from_ip"

      t.string   :confirmation_token
    end

    add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  end
end
