class CreateAccountRoleRights < ActiveRecord::Migration[5.0]
  def change
    create_table "account_role_rights", force: true do |t|
      t.integer "account_role_id"
      t.integer "right_id"
    end
    add_index "account_role_rights", ["account_role_id"], name:"index_account_role_rights_on_account_role_id", using: :btree
  end
end
