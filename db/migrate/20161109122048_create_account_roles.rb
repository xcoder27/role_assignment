class CreateAccountRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :account_roles do |t|
      t.references :account, foreign_key: true
      t.string :name
      t.timestamps
    end
    add_index "account_roles", ["id", "account_id"], name:"index_account_roles_on_id_and_account_id", using: :btree
  end
end
