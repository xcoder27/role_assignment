class CreateRights < ActiveRecord::Migration[5.0]
  def change
    create_table "rights", force: true do |t|
      t.string "key"
      t.string "name"
      t.string "category"
      t.string "actions", limit: 10240
    end
    add_index "rights", ["key"], name: "index_rights_on_key", using: :btree
  end
end
